import store from '@/store/index'

let ws

const methods = {
	wsRequest: () => {
		console.log(store.state.userData)
		const urlString = 'ws://localhost:7070/ws?user=' + store.state.userData.id
		ws = new WebSocket(urlString)
		if (ws.readyState !== 0 && ws.readyState !== 1) {
			return false
		}
		return true
	},
	listenToWs: () => {
		ws.addEventListener('message', function (e) {
			let msg = JSON.parse(e.data)
			store.commit(
				'receiveMessage',
				msg
			)
		})
	},
	writeToWs: (data) => {
		ws.send(data)
	}
}

const wsApi = {
	methods: methods
}

export default wsApi