import Vue from 'vue'
import Router from 'vue-router'
import chatList from '@/layouts/chatList'
import chatPane from '@/layouts/chatPane'
import auth from '@/layouts/auth'
import signUp from '@/layouts/signUp'
import store from '@/store/index'

Vue.use(Router)

const routes = new Router({
	routes: [
		{
			path: '/',
			name: 'chatList',
			component: chatList,
			children: [
				{
					path: '/chat/:chatId',
					component: chatPane
				}
			]
		},
		{
			path: '/login',
			name: 'auth',
			component: auth
		},
		{
			path: '/signup',
			name: 'signUp',
			component: signUp
		}
	]
})

routes.beforeEach((to, from, next) => {
	if (!store.state.authentication
		&& to.path !== '/login'
		&& to.path !== '/signup') {
			next('/login')
	} else {
		if (to.path === 'login') {
			next('/')
		}
		next()
	}
})

export default routes
