import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/utils/requests'

Vue.use(Vuex)

const state = {
	authentication: false,
	errors: [],
	userId: '',
	userData: null,
	messageContent: '',
	chats: {
		'1': {
			'name': 'Group A',
			'messages': [],
			'notifications': 0
		},
		'2': {
			'name': 'Group B',
			'messages': [],
			'notifications': 0
		}
	}
}

const getters = {
	chatList: state => {
		if (!state.chats) return {}
		let chats = JSON.stringify(state.chats)
		chats = JSON.parse(chats)
		return chats
	},
	userData: state => {
		return state.userData
	}
}

const mutations = {
	updateMessage(state, content) {
		state.messageContent = content
	},
	receiveMessage(state, msg) {
		state.chats[msg['chatId']]['messages'].push({
			'message': msg.message,
			'userId': msg.userId,
			'time': msg.date
		})
	},
	setJWT(state, jwt) {
		let arr = jwt.split('.')
		let payload = atob(arr[1])
		state.userData = JSON.parse(payload)
	},
	updateAuth(state, value) {
		state.authentication = value
	},
	addGlobalError(state, e) {
		state.errors.push(e.message)
	}
}

const actions = {
	postAuth: (context, data) => {
		return api.post('http://localhost:7070/signup', data)
			.then((r) => {
				context.commit('setJWT', r.data)
			})
			.catch((error) => context.commit('addGlobalError', error));
	}
};

let store = new Vuex.Store({
	state,
	mutations,
	getters,
	actions
})

export default store